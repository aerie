#include "helpers.h"


/**
 *
 */
string urlencode(string s)
{
	int length = s.length();
	string out;
	char tmp[10];
	
	for (int i=0; i<length; i++)
		if ((s[i]>='a' && s[i]<='z')
		|| (s[i]>='A' && s[i]<='Z')
		|| (s[i]>='0' && s[i]<='9')
		|| s[i]=='-' 
		|| s[i]=='_')
			out += s[i];
		else
		{
			sprintf(tmp, "%%%02x", (unsigned char)s[i]);
			out += tmp;
		}
		
		return out;
}

string urldecode(string s)
{
	string out;

	int number = 0;
	int len = s.length();
	char tmp[2] = " ";

	for (int i=0; i<len; i++)
	{
		switch (s[i]) 
		{
			case '%':
				number = 0;
				for (int j=1; j<=2; j++)
				{
					number *= 16;
					if (s[i+j]>='A' && s[i+j]<='F')
						number += 10 + s[i+j] - 'A';
					else if (s[i+j]>='a' && s[i+j]<='f')
						number += 10 + s[i+j] - 'a';
					else
						number += s[i+j] - '0';
				}
				tmp[0] = number;
				out += tmp;
				i+=2;
			break;

			default:
				out += s[i];
		}
	}

	return out;
}

