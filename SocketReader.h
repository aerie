#pragma once

#include "definitions.h"

class CSocketReader
{
private:
	const int TEMP_READ_BUFFER_SIZE;

	char*	buffer;
	int		bufferSize;
	SOCKET	socket;
	int		readedPointer;

public:
	CSocketReader(SOCKET s);
	~CSocketReader(void);

	/**
	 * przerobic na wczytywanie az do otrzymania konkretnego znaku?
	 */
	bool	read();
	int		getData(int size);
	void	setPointer(int p) { readerPointer = p; }
	int		changePointer(int p) { readerPointer += p; return readerPointer; }
	char*	getCharacter() { if (readedPointer>=bufferSize) return NULL; return &buffer[readedPointer++]; }
};
