#include "SocketReader.h"

CSocketReader::CSocketReader(SOCKET s) 
	: buffer(NULL), bufferSize(0), readedPointer(0), TEMP_READ_BUFFER_SIZE(1024), socket(s)
{}

CSocketReader::~CSocketReader(void)
{
	SAFE_DELETE_ARRAY(buffer);
}

bool CSocketReader::read()
{
	char *buf = new char[TEMP_READ_BUFFER_SIZE];
	int readed = 0;

	while ((readed = recv(socket, buf, TEMP_READ_BUFFER_SIZE, 0/*MSG_DONTWAIT*/)) > 0)
	{
		buffer = (char*)realloc(buffer, bufferSize+readed);
		memcpy( &buffer[bufferSize], buf, readed);
	}

	SAFE_DELETE_ARRAY(buf);
	return true;
}

int CSocketReader::getData(int size, char* buff)
{
	/**
	 * \todo implement
	 */
	return 0;
}

