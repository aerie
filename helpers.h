#ifndef _HELPERS_H_
#define _HELPERS_H_

#include <string>

using namespace std;

string urlencode(string s);
string urldecode(string s);

#endif //_HELPERS_H_