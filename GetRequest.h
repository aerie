#pragma once

#include "definitions.h"
#include "IRequest.h"

class CGetRequest :
	public IRequest
{
public:
	CGetRequest(SOCKET s);
	virtual ~CGetRequest(void);
};
