#pragma once

#include <string>
#include <vector>

#include "definitions.h"

/**
 * Base class for all requests.
 * All request classes must inherits from this.
 */
typedef std::vector<std::string> strings;

/**
 *
 *
 */
class IRequest
{
protected:
	/**
	 * ordered list of all headers from request
	 */
	strings headersStrings;
	strings	headersNames;
	strings	headersValues;

	/**
	 * binary data sent with request
	 */
	char*	data;

public:
	IRequest(SOCKET s);
	virtual ~IRequest(void);

	/**
	 * Initiate request
	 */
	virtual bool init() = 0;
	virtual bool finish() = 0;
	virtual bool isWaitingForSocket() = 0;
	virtual bool addHeader(std::string header) = 0;
	virtual bool setHeaders(strings headers) = 0;
	virtual bool startProcess() = 0;
	virtual bool test() = 0;
};
