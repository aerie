#ifndef _AERIE_DEFINITIONS_H_
#define _AERIE_DEFINITIONS_H_
/**
 * Global definitions needed globaly in project.
 */


/**
 * CONFIGURATION
 */
#define CONF_TCP_QUEUE_SIZE 5


/*
 * new types definitions
 */
// typedef int SOCKET;


#include <winsock2.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * useful macros
 */
#define SAFE_DELETE(a) if (a) delete a; a = NULL
#define SAFE_DELETE_ARRAY(a) if (a) delete[] a; a = NULL

#endif //_AERIE_DEFINITIONS_H_