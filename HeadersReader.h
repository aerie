#pragma once

#include "SocketReader.h"
#include <string>

struct SHeader {
	std::string line;
	std::string	name;
	std::string	value;
};

class CHeadersReader
{
private:
	CSocketReader*	reader;
	SHeader			lastHeader;

public:
	CHeadersReader(CSocketReader *r);
	~CHeadersReader(void);

	SHeader*	readHeader();
};
