// aerie.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "definitions.h"
#include "helpers.h"

/**
 * windows version
 *
 * \todo obs�uga b��d�w!
 */
SOCKET win_start_server(int port)
{
	WORD ver;
	WSADATA data;
	SOCKET s;
	sockaddr_in addr;


	ver = MAKEWORD(2,0);
	if (WSAStartup(ver,NULL) != 0)
	{
		s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		memset(&addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.s_addr = htonl(INADDR_ANY);

		int foo = 1;
		setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char*)&foo, sizeof(foo));

		bind(s, (sockaddr*)&addr, sizeof(addr));

		listen(s, CONF_TCP_QUEUE_SIZE);

		return s;
	}

	return 0;
}

int main(int argc, char* argv[])
{
	std::string s;
	s = "test123_!@#$%^&*()_+ za���";
	printf("[%s]\n", s.c_str());
	s = urlencode(string(s));
	printf("[%s]\n", s.c_str());
	s = urldecode(s);
	printf("[%s]\n", s.c_str());
	return 0;
}


/*

MAIN_LOOP:
server_loop -> open_socket -> socket_reader

socket_reader -> request_factory -> request

request -> queue(read data for request - type of task)


WORKER:
queue -> get needed data -> queue(send)

	
[task] : property of request(?)
class task {
	bool run() - TRUE task is finished, FALSE needs to remain in queue
}

class getFileTask : public task {
	bool run() {}
	char* getData()
}

-- [code] ------------------------------------------------------------------------------
:: obsluga kolejki zadan od zadan

queue< pair<request,task> > tasks_queue;
while !empty(task_queue)
	pair = front(tasks_queue)
	if (pair.task.run())
		requests_queue.add(pair.request)
	else
		tasks_queue.add(pair);
-- [/code] -----------------------------------------------------------------------------


-- [code] ------------------------------------------------------------------------------
:: obsluga czytania socketow

if (some_socket_need_to_be_read)
	socket = find_socket_to_read()
	reader = get_socket_reader(socket)
	reader.read()
-- [/code] -----------------------------------------------------------------------------


*/
